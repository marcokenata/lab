package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Cto extends Employees {
    public Cto(String name,Double salary) {
        this.name = name;
        if (salary < 100000.00) {
            throw new IllegalArgumentException("Cto salary must not lower than 200000.00");
        }
        this.salary = salary;
        this.role = "CTO";
    }

    @Override
    public double getSalary() {
        return this.salary;
    }
}
