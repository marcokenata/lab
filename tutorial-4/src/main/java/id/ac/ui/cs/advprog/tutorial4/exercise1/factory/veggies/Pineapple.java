package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

public class Pineapple implements Veggies {

    @Override
    public String toString() {
        return "Pineapple";
    }
}
