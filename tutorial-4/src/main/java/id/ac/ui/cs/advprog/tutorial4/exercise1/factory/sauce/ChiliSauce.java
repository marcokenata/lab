package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

public class ChiliSauce implements Sauce {

    @Override
    public String toString() {
        return "Chili Sauce";
    }
}
