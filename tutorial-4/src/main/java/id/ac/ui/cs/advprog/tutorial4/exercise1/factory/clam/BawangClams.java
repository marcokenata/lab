package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class BawangClams implements Clams {

    @Override
    public String toString() {
        return "Bawang clams from New Zealand";
    }
}
