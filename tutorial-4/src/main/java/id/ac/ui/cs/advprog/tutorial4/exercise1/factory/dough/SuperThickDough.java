package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class SuperThickDough implements Dough {

    @Override
    public String toString() {
        return "Super Thick Indonesian Style Dough";
    }
}
