package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

public class CreamCheese implements Cheese {

    @Override
    public String toString() {
        return "Cream Cheese";
    }
}
