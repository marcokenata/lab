package id.ac.ui.cs.advprog.tutorial4.exercise2;

public class Singleton {

    private static Singleton obj;

    private Singleton(){}

    public static Singleton getInstance() {
        if (obj == null) {
            obj = new Singleton();
        }

        return obj;
    }
}
