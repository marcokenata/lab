package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.DepokPizzaStore;
import id.ac.ui.cs.advprog.tutorial4.exercise1.NewYorkPizzaStore;
import id.ac.ui.cs.advprog.tutorial4.exercise1.PizzaStore;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;

import org.junit.Before;
import org.junit.Test;

public class FactoryTest {

    protected PizzaStore nyPizzaStore;
    protected PizzaStore depokPizzaStore;

    @Before
    public void setUp() {
        nyPizzaStore = new NewYorkPizzaStore();
        depokPizzaStore = new DepokPizzaStore();
    }

    @Test
    public void orderPizza() {
        Pizza pizza;

        pizza = nyPizzaStore.orderPizza("cheese");
        assertEquals("New York Style Cheese Pizza",pizza.getName());

        pizza = nyPizzaStore.orderPizza("veggie");
        assertEquals("New York Style Veggie Pizza",pizza.getName());

        pizza = nyPizzaStore.orderPizza("clam");
        assertEquals("New York Style Clam Pizza",pizza.getName());

        pizza = depokPizzaStore.orderPizza("cheese");
        assertEquals("Depok Style Cheese Pizza",pizza.getName());

        pizza = depokPizzaStore.orderPizza("veggie");
        assertEquals("Depok Style Veggie Pizza",pizza.getName());

        pizza = depokPizzaStore.orderPizza("clam");
        assertEquals("Depok Style Clam Pizza",pizza.getName());
    }
}
