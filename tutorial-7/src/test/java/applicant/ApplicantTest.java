package applicant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;


public class ApplicantTest {
    private Applicant applicant = new Applicant();

    @Test
    public void testIsCredible() {
        assertTrue(applicant.isCredible());
    }

    @Test
    public void testHasCriminalRecord() {
        assertTrue(applicant.hasCriminalRecord());
    }

    @Test
    public void testGetCreditScore() {
        assertEquals(applicant.getCreditScore(),700);
    }

    @Test
    public void testGetEmploymentYears() {
        assertEquals(applicant.getEmploymentYears(),10);
    }

    @Test
    public void testMain() {
        Applicant.main(new String[]{});
    }
}
