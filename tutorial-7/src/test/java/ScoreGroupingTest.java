import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class ScoreGroupingTest {

    private static Map<String, Integer> scores = new HashMap<>();

    @Test
    public void testMain() {
        ScoreGrouping.main(new String[]{});
    }

    @Before
    public void setUp() {
        scores.put("Kelly",9);
        scores.put("Hehe",11);
        scores.put("Qewew",13);
    }

    @Test
    public void printAll() {
        assertEquals("{9=[Kelly], 11=[Hehe], 13=[Qewew]}",
                ScoreGrouping.groupByScores(scores).toString());
    }

}