package tallycounter;

public class SyncTallyCount {
    private int counter = 0;

    public synchronized void increase() {
        counter++;
    }

    public synchronized void decrease() {
        counter--;
    }

    public synchronized int value() {
        return counter;
    }
}
