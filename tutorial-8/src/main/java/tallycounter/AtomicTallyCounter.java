package tallycounter;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicTallyCounter {
    private AtomicInteger counter = new AtomicInteger(0);

    public void increase() {
        counter.getAndIncrement();
    }

    public void decrease() {
        counter.getAndDecrement();
    }

    public int value() {
        return counter.get();
    }
}
