package game;

public class QuizTime implements Runnable {
    private QuizSc scorel;
    private Thread thread;
    private final int sleep = 100;

    public QuizTime(QuizSc scorel) {
        this.scorel = scorel;
    }

    public void start() {
        if (thread == null) {
            thread = new Thread(this,"Tally Counter Checker");
            thread.start();
        }
    }


    @Override
    public void run() {
        try {
            while (true) {
                Thread.sleep(sleep);
                scorel.decrease();
            }
        } catch (InterruptedException e) {
            System.out.println("Quiz timer interrupted");
        }
        System.out.println("Quiz timer stopped");
    }
}
