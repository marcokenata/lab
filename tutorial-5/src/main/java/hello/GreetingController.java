package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GreetingController {

    @GetMapping("/greeting")
    public String greeting(@RequestParam(name = "name", required = false)
                                       String name, Model model) {
        if (name == null || name.equals("")) {
            model.addAttribute("title", "This is my CV");
        } else {
            model.addAttribute("title", name + ", I hope you are interested to hire me");
        }

        StringBuilder cv = new StringBuilder();
        cv.append("Name: Marco Kenata\n");
        cv.append("Birthdate: 26/09/1998\n");
        cv.append("Birthplace: Medan\n");
        cv.append("Address: Apartemen Margonda Residence C 302 A\n");
        cv.append("Education History:\n");
        cv.append("- SMP Sutomo 1 Medan 2010-2013\n");
        cv.append("- SMA Sutomo 1 Medan 2013-2016\n");
        cv.append("- Faculty of Computer Science 2016-Present\n");
        model.addAttribute("cv", cv.toString());

        StringBuilder description = new StringBuilder();
        description.append("I am a second-year CS student at Universitas Indonesia. ");
        description.append("Interests include memes, EDM and also Web Programming");
        model.addAttribute("description", description.toString());

        return "greeting";
    }

}
