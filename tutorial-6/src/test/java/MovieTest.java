import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class MovieTest {
    private Movie movie;

    @Before
    public void setUp() {
        movie = new Movie("Captain Underwear",Movie.REGULAR);
    }

    @Test
    public void getTitle() {
        assertEquals("Captain Underwear", movie.getTitle());
    }

    @Test
    public void setTitle() {
        movie.setTitle("Mamak Kau");

        assertEquals("Mamak Kau", movie.getTitle());
    }

    @Test
    public void getPriceCode() {
        assertEquals(Movie.REGULAR, movie.getPriceCode());
    }

    @Test
    public void setPriceCode() {
        movie.setPriceCode(Movie.CHILDREN);
        assertEquals(Movie.CHILDREN, movie.getPriceCode());
    }

    @Test
    public void checkHash() {
        assertNotNull(movie.hashCode());
    }

    @Test
    public void checkEquals() {
        Movie movie1 = null;
        assertFalse(movie.equals(movie1));

        movie1 = new Movie("Captain Underwear",Movie.REGULAR);
        assertTrue(movie.equals(movie1));
    }
}