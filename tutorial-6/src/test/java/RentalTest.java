import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class RentalTest {
    private Movie movie;
    private Rental rental;

    @Before
    public void setUp() {
        movie = new Movie("Setarbuk",Movie.REGULAR);
        rental = new Rental(movie, 4);
    }

    @Test
    public void getMovie() {
        assertEquals(movie, rental.getMovie());
    }

    @Test
    public void getDaysRented() {
        assertEquals(4, rental.getDaysRented());
    }
}